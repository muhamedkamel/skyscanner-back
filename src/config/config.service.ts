import * as dotenv from 'dotenv';
import * as Joi from '@hapi/joi';
import * as fs from 'fs';

export interface EnvConfig {
  [key: string]: string | number;
}

export class ConfigService {
  private readonly env: EnvConfig;
  private static instance = null;

  constructor(filePath: string) {
    const config = dotenv.parse(fs.readFileSync(filePath));
    this.env = this.validateInput(config);
  }

  static getInstance(): ConfigService {
    if (this.instance == null) {
      this.instance = new ConfigService(`${process.env.NODE_ENV || ''}.env`);
    }

    return this.instance;
  }

  get(key: string): string | number {
    return this.env[key];
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(env: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      APP_PORT: Joi.number().default(3000),
      DB_CONNECTION: Joi.string().required(),
      DB_HOST: Joi.string().required(),
      DB_PORT: Joi.number().required(),
      DB_USERNAME: Joi.string().required(),
      DB_PASSWORD: Joi.string().required(),
      DB_DATABASE: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      env,
      envVarsSchema,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }
}

const config = ConfigService.getInstance();

export { config };

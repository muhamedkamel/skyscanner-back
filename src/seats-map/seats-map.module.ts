import { Module } from '@nestjs/common';
import { SeatsMapController } from './seats-map.controller';
import { SeatsMapService } from './seats-map.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SeatsMapSchema } from '../infrastructure/schemas/seats-map.schema';
import { SeatsMapRepository } from './seats-map.repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'SeatsMap', schema: SeatsMapSchema },
    ]),
  ],
  controllers: [SeatsMapController],
  providers: [SeatsMapService, SeatsMapRepository],
})
export class SeatsMapModule { }

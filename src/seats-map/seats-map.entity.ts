import { Exclude } from 'class-transformer';
import { SeatsMap } from '../interfaces/seats-map.interface';

export class SeatsMapEntity {
    @Exclude()
    '_id': string;

    planId: string;
    map: SeatsMap;
    createdAt: Date;
    updatedAt: Date;

    @Exclude()
    '__v': number;

    constructor(partial: Partial<SeatsMapEntity>) {
        Object.assign(this, partial);
    }
}

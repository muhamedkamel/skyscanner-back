import { Controller, Param, Get, Post, Body, Patch, UseInterceptors, ClassSerializerInterceptor, Response, HttpStatus } from '@nestjs/common';
import { SeatsMapService } from './seats-map.service';
import { SeatsMapEntity } from './seats-map.entity';
import { MongooseDocument } from 'mongoose';
import { Seat } from '../interfaces/seat.interface';
import { serialize } from 'class-transformer';

@Controller('seats-map')
export class SeatsMapController {

    constructor(private seatsMapService: SeatsMapService) { }

    @UseInterceptors(ClassSerializerInterceptor)
    @Get(':planId')
    public async getSeats(@Param('planId') planId: string) {
        const seatsMap = await this.seatsMapService.getMap(planId) as MongooseDocument;

        return new SeatsMapEntity(seatsMap.toJSON());
    }

    @Post()
    public createSeatsMap(@Body() seatsMap) {
        return this.seatsMapService.createSeatsMap(seatsMap);
    }

    @Patch(':planId')
    public async reserveSeats(@Param('planId') planId: string, @Body() seats: Seat[], @Response() response) {
        // @TODO use the default response and create a custom decorator for the failure case
        try {
            const updatedSeatsMap = await this.seatsMapService.reserveSeats(planId, seats) as MongooseDocument;
            const body = serialize(new SeatsMapEntity(updatedSeatsMap.toJSON()));

            response.status(HttpStatus.OK).json(JSON.parse(body));
        } catch (e) {
            const seatsMap = await this.seatsMapService.getMap(planId) as MongooseDocument;
            const body = serialize(new SeatsMapEntity(seatsMap.toJSON()));

            response.status(HttpStatus.PRECONDITION_FAILED).json(JSON.parse(body));
        }
    }
}

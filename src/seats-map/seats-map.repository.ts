import { InjectModel } from '@nestjs/mongoose';
import { Model, MongooseDocument } from 'mongoose';
import { SeatsMap } from '../interfaces/seats-map.interface';
import { Injectable } from '@nestjs/common';
import { Seat } from '../interfaces/seat.interface';

@Injectable()
export class SeatsMapRepository {
    constructor(@InjectModel('SeatsMap') private readonly seatsMap: Model<SeatsMap>) { }

    public async findByPlanId(planId: string): MongooseDocument {
        return await this.seatsMap.findOne({ planId }).sort({ createdAt: -1 });
    }

    public create(seatsMap) {
        return this.seatsMap.create(seatsMap);
    }

    public async update(seatsMap: MongooseDocument, seats: Seat[]) {

        for (const seat of seats) {
            const row = seat.row;
            const column = seat.column - 1;

            seatsMap.map[row][column].reserved = true;
        }

        seatsMap.markModified('map');

        return await seatsMap.save();
    }
}

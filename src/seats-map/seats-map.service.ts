import { Injectable, ConflictException } from '@nestjs/common';
import { SeatsMapRepository } from './seats-map.repository';
import { Seat } from '../interfaces/seat.interface';
import { SeatsMap } from '../interfaces/seats-map.interface';
import { MongooseDocument } from 'mongoose';

@Injectable()
export class SeatsMapService {

    constructor(private readonly seatsMapRepo: SeatsMapRepository) { }

    public getMap(planId: string) {
        return this.seatsMapRepo.findByPlanId(planId);
    }

    public createSeatsMap(seatsMap) {
        return this.seatsMapRepo.create(seatsMap);
    }

    public async reserveSeats(planId: string, seats: Seat[]) {
        const seatsMap = await this.seatsMapRepo.findByPlanId(planId) as MongooseDocument;

        if (this.isAnySeatReserved(seatsMap, seats)) {
            throw new ConflictException();
        }

        return this.seatsMapRepo.update(seatsMap, seats);
    }

    public isAnySeatReserved(seatsMap: SeatsMap, seats: Seat[]) {
        for (const seat of seats) {
            const column = seat.column - 1;
            const row = seat.row;

            if (seatsMap.map[row][column].reserved === true) {
                return true;
            }
        }

        return false;
    }
}

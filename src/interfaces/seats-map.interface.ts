import { Seat } from './seat.interface';

export interface SeatsMap {
    [key: string]: Seat[];
}

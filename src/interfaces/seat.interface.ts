export interface Seat {
    reserved: boolean;
    row: string;
    column: number;
}

import * as mongoose from 'mongoose';
import * as MongooseUpdateVersioning from 'mongoose-update-versioning';

export const SeatsMapSchema = new mongoose.Schema(
    {
        planId: { type: String, unique: true },
        map: { type: Object },
    },
    {
        timestamps: {
            createdAt: true,
            updatedAt: true,
        },
    },
).plugin(MongooseUpdateVersioning);

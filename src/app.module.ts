import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SeatsMapModule } from './seats-map/seats-map.module';
import { ConfigModule } from './config/config.module';
import { config } from './config/config.service';

const db = config.get('DB_CONNECTION') + `://` + config.get('DB_HOST') + `:` + config.get('DB_PORT');

@Module({
  imports: [
    ConfigModule,
    SeatsMapModule,
    MongooseModule.forRoot(db, {
      user: config.get('DB_USERNAME'),
      pass: config.get('DB_PASSWORD'),
      dbName: config.get('DB_DATABASE'),
      useNewUrlParser: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }

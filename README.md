## SkyScanner
This project was built with [Nest](https://github.com/nestjs/nest)

## Prerequisets
- node v8.10.0
- tsc v2.7.2
- mongodb v3.6.12

## Installation

```bash
$ git clone https://muhamedkamel@bitbucket.org/muhamedkamel/skyscanner-back.git
$ cd skyscanner-back
$ npm install
```

## Configurations
> create a `.env` file in the project root directory.

```sh
APP_PORT=3000

# mongodb
DB_CONNECTION=mongodb
DB_HOST=localhost
DB_PORT=27017
DB_USERNAME=username
DB_PASSWORD=password
DB_DATABASE=skyscanner
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Initializing the db with the seats map
> You can use postman

- url: `localhost:3000/api/seats-map`
- method: `POST`
- headers: `[Content-Type: application/json]`
- body: 
```json
{
    "planId": 1,
    "map": {
        "A": [
            {
                "reserved": true,
                "row": "A",
                "column": 1
            },
            {
                "reserved": false,
                "row": "A",
                "column": 2
            },
            {
                "reserved": false,
                "row": "A",
                "column": 3
            },
            {
                "reserved": false,
                "row": "A",
                "column": 4
            },
            {
                "reserved": false,
                "row": "A",
                "column": 5
            },
            {
                "reserved": false,
                "row": "A",
                "column": 6
            },
            {
                "reserved": false,
                "row": "A",
                "column": 7
            },
            {
                "reserved": false,
                "row": "A",
                "column": 8
            },
            {
                "reserved": false,
                "row": "A",
                "column": 9
            },
            {
                "reserved": false,
                "row": "A",
                "column": 10
            }
        ],
        "B": [
            {
                "reserved": false,
                "row": "B",
                "column": 1
            },
            {
                "reserved": false,
                "row": "B",
                "column": 2
            },
            {
                "reserved": false,
                "row": "B",
                "column": 3
            },
            {
                "reserved": false,
                "row": "B",
                "column": 4
            },
            {
                "reserved": false,
                "row": "B",
                "column": 5
            },
            {
                "reserved": false,
                "row": "B",
                "column": 6
            },
            {
                "reserved": false,
                "row": "B",
                "column": 7
            },
            {
                "reserved": false,
                "row": "B",
                "column": 8
            },
            {
                "reserved": false,
                "row": "B",
                "column": 9
            },
            {
                "reserved": false,
                "row": "B",
                "column": 10
            }
        ],
        "C": [
            {
                "reserved": false,
                "row": "C",
                "column": 1
            },
            {
                "reserved": false,
                "row": "C",
                "column": 2
            },
            {
                "reserved": false,
                "row": "C",
                "column": 3
            },
            {
                "reserved": false,
                "row": "C",
                "column": 4
            },
            {
                "reserved": false,
                "row": "C",
                "column": 5
            },
            {
                "reserved": false,
                "row": "C",
                "column": 6
            },
            {
                "reserved": false,
                "row": "C",
                "column": 7
            },
            {
                "reserved": false,
                "row": "C",
                "column": 8
            },
            {
                "reserved": false,
                "row": "C",
                "column": 9
            },
            {
                "reserved": false,
                "row": "C",
                "column": 10
            }
        ],
        "D": [
            {
                "reserved": false,
                "row": "D",
                "column": 1
            },
            {
                "reserved": false,
                "row": "D",
                "column": 2
            },
            {
                "reserved": false,
                "row": "D",
                "column": 3
            },
            {
                "reserved": false,
                "row": "D",
                "column": 4
            },
            {
                "reserved": false,
                "row": "D",
                "column": 5
            },
            {
                "reserved": false,
                "row": "D",
                "column": 6
            },
            {
                "reserved": false,
                "row": "D",
                "column": 7
            },
            {
                "reserved": false,
                "row": "D",
                "column": 8
            },
            {
                "reserved": false,
                "row": "D",
                "column": 9
            },
            {
                "reserved": false,
                "row": "D",
                "column": 10
            }
        ],
        "E": [
            {
                "reserved": false,
                "row": "E",
                "column": 1
            },
            {
                "reserved": false,
                "row": "E",
                "column": 2
            },
            {
                "reserved": false,
                "row": "E",
                "column": 3
            },
            {
                "reserved": false,
                "row": "E",
                "column": 4
            },
            {
                "reserved": false,
                "row": "E",
                "column": 5
            },
            {
                "reserved": false,
                "row": "E",
                "column": 6
            },
            {
                "reserved": false,
                "row": "E",
                "column": 7
            },
            {
                "reserved": false,
                "row": "E",
                "column": 8
            },
            {
                "reserved": false,
                "row": "E",
                "column": 9
            },
            {
                "reserved": false,
                "row": "E",
                "column": 10
            }
        ],
        "F": [
            {
                "reserved": false,
                "row": "F",
                "column": 1
            },
            {
                "reserved": false,
                "row": "F",
                "column": 2
            },
            {
                "reserved": false,
                "row": "F",
                "column": 3
            },
            {
                "reserved": false,
                "row": "F",
                "column": 4
            },
            {
                "reserved": false,
                "row": "F",
                "column": 5
            },
            {
                "reserved": false,
                "row": "F",
                "column": 6
            },
            {
                "reserved": false,
                "row": "F",
                "column": 7
            },
            {
                "reserved": false,
                "row": "F",
                "column": 8
            },
            {
                "reserved": false,
                "row": "F",
                "column": 9
            },
            {
                "reserved": false,
                "row": "F",
                "column": 10
            }
        ]
    }
}
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil My�liwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
